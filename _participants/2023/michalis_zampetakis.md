---
full_name: Michalis Zampetakis
image: mzampetakis.jpeg
website: https://mzampetakis.com/
linkedin: https://www.linkedin.com/in/mzampetakis/
twitter: https://twitter.com/mzampetakis
github: https://github.com/mzampetakis
---
