---
layout: post 
title: Tickets and Venue 
description: See all packages - accommodation optionally included! 
image: socrates_crete_2022_balos.jpeg
---


<h2>SoCraTes Crete 2023</h2>

<p>After 4 iterations of AgileCrete and 3 iterations of AgileCrete + SoCraTes Crete, in 2023 SoCrates Crete becomes its own event this summer in Chania, Crete!! 🎉&nbsp;🥳&nbsp;🚀</p>

![Crete, Chania, Balos]({{ site.baseurl }}/assets/images/crete_chania_balos.jpg){: width="1024" max-width=100% }


<p>SoCraTes Crete will take place in the same familiar setting, that AgileCrete has been running in combining special character and an amazing location!</p>
<p>Some locals will claim (and others will contest! ) that <strong>Chania is the most beautiful part of Crete</strong>. We’d like to leave that up to you to decide for yourselves</p>

<h2>A Retreat in a Dream Setting</h2>

<p>Located in the small village of Kolymbari, a sea-side village, there is a conference center that almost goes out of its way to stay <em>just</em> a conference center and keep tourists away (please <strong>do not underestimate</strong> how hard that is to do on Crete!).</p>

<p>The <a href="https://www.oac.gr/en/">Orthodox Academy of Crete</a> allows SoCraTes Crete to function as a kind of <strong>retreat</strong> – like several of the other <strong>SoCraTes</strong> events – while keeping everything else that made it an event you’ve all enjoyed in the previous years.</p>


![Not a bad spot for a bit of coding, don’t you think?]({{ site.baseurl }}/assets/images/socrates_crete_2022_coding_kata.jpeg){: width="1024" }
<p>Not a bad spot for a bit of coding, don’t you think?</p>


<p><strong>All our participants have the option to stay on the same venue,</strong> where the event will run (until rooms run out!), allowing for more facetime with each other and a much richer knowledge exchange that we expect will happen.</p>

<h2>Accommodation</h2>

<p>The venue itself is literally <strong>on the other side of the road from a quiet, secluded beach</strong> that only some locals use (read: no sunbeds), overlooking the gulf of Kolymbari. You wake up early enough and you can get to <strong>see the sun rise from the sea.</strong>
</p>

<p>Inspiration? Guaranteed!</p>

![Inspiration? Guaranteed!]({{ site.baseurl }}/assets/images/socrates_crete_2022_4.jpeg){: width="1024" }

<p>
<strong>Accommodation is optionally included</strong>, for those of you that choose to stay on site (there are other options in Kolymbari as well), so there’s less fuss all around about what to book, how to get to the venue (and back) etc.
</p>

<h3>Not-for-profit Event</h3>

<p>
All the packages below are offered <strong>directly</strong> by the OAC themselves. You do not pay us (the Org Team), you pay the venue directly (and we do not take any share of this money). SoCraTes Crete is a <strong>not-for-profit event</strong> that is funded by its participants. <strong>We all pay (including the Org Team)</strong>, to cover the venue + catering costs for the event. You can see our <a href="https://opencollective.com/socrates-crete" target="_blank">Open Collective Page</a> for more details!
</p>

<h3><a id="full-packages"></a>Abvailable Packages</h3>

<p>
Some of the packages below include accommodation, breakfast, lunch and coffee breaks at the venue, with <strong>arrival (and check-in at the OAC) on Thursday 29th June</strong> <strong>and departure (check-out) on Sunday, 2nd July</strong>.
</p>

<p><em>1. Conference + Coffee breaks: 150 €</em></p>
<p><em>2. Conference + Coffee breaks + Accommodation at the OAC (sharing double room): 300 €</em></p>
<p><em>3. Conference + Coffee breaks + Accommodation at the OAC (single room): 400 €</em></p>
<p><em>4. Accompanying adult person, sharing a room at OAC 200 €</em></p>
<p><em>5. Accompanying under aged ( > 6 y.o.), sharing a room at OAC: 135 €</em></p>
<p><em>6. Accompanying under aged ( < 6 y.o.), sharing a room at OAC: 0 € </em></p>

>All accommodation packages include breakfast and lunch at the OAC. 

![Full Packages]({{ site.baseurl }}/assets/images/socrates_crete_2022_3.jpeg){: width="1024" }

<h2><a id="how-to-book"></a>How to Book</h2>

<p>Registration is currently closed. Stay tunned for SoCraTes Crete 2024 edition!
<!-- Please use this <a href="https://forms.zohopublic.eu/socratescrete/form/SoCraTesShowusyourinterestform/formperma/5A4vRR2hoQP2eo0la_Sq7qhUaUgQJV26zoNmVCvdE-s" target="_blank">registration form</a> if you’re considering joining us this year, so you will hear all the news about the event! 
Payment will be requested at a later time, (Note: the form is externally hosted on <a href="https://www.zoho.com/">ZoHo Forms</a>, since this here is only a static site) .
-->
<br />
<br />

If you have questions regarding the booking please <a href="https://discord.gg/4Z45eHvZDJ">join us on Discord</a> or check our <a href="{{ '/faq.html' | relative_url }}">FAQ page</a>.</p>

<h2>Alternatives</h2>

<p>
Apart from the OAC, Kolympari also offers some 5-star resorts, and some rooms you can find on platforms like AirBnB or Booking.com, etc.
</p>

<p>
Walking to the OAC from the town of Kolympari is certainly an option, but you’ll want to avoid the 15 min walk on a hot day under the midday sun (we do NOT recommend you try that, especially if you’re not used to hot weather). Temperatures in June are usually not as high as they get in mid-July, or August, but we’d still recommend hiring / sharing a car instead!
</p>

<p>
    By the way: <strong>Participants traditionally share cars</strong>, enjoy giving others a ride to the venue and are a lovely bunch of helpful individuals! <a href="mailto:{{site.email}}" target="_blank">Drop us an email,</a> or <a href="{{site.discord_url}}" target="_blank">join us on Discord</a>!
</p>

<style type="text/css">
img {
    max-width: 100%;
}
</style>
